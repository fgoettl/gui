from __future__ import annotations

from typing import NamedTuple


class Color(NamedTuple):
    r: float
    g: float
    b: float
    a: float


WHITE = Color(1.0, 1.0, 1.0, 1.0)
BLACK = Color(0.0, 0.0, 0.0, 1.0)
RED = Color(1.0, 0.0, 0.0, 1.0)
GREEN = Color(0.0, 1.0, 0.0, 1.0)
BLUE = Color(0.0, 0.0, 1.0, 1.0)
GRAY30 = Color(0.3, 0.3, 0.3, 1.0)


def mix(c1: Color, c2: Color) -> Color:
    r1, g1, b1, a1 = c1
    r2, g2, b2, a2 = c2
    return Color((r1 + r2) / 2, (g1 + g2) / 2, (b1 + b2) / 2, (a1 + a2) / 2)
