#version 430 core

layout (location = 0) in int id;
layout (location = 1) in ivec2 position;
layout (location = 2) in vec4 xcolor;
layout (location = 3) in ivec2 size;

struct global_record{
    int parent;
    int dx;
    int dy;
    int rel_depth;
    int br_xmin;
    int br_xmax;
    int br_ymin;
    int br_ymax;
    int desc_vis;
};

layout (std430, binding = 0) buffer _globals{
    readonly global_record g[];
} globals;

uniform int _window_width;
uniform int _window_height;

struct recursive_info{
    int x_trafo;
    int y_trafo;
    int relative_depth;
    int br_xmin;
    int br_xmax;
    int br_ymin;
    int br_ymax;
    int vis;
};
recursive_info calculate_recursives(int id){
    int i = id;
    int dx = globals.g[i].dx;
    int dy = globals.g[i].dy;
    int cdx = dx;
    int cdy = dy;
    int rd = globals.g[i].rel_depth;
    int br_xmin = -214748364;
    int br_xmax = 214748364;
    int br_ymin = -214748364;
    int br_ymax = 214748364;
    int vis = globals.g[i].desc_vis;

    while(globals.g[i].parent != i)
    {
        i = globals.g[i].parent;
        br_xmin = cdx + br_xmin >= globals.g[i].br_xmin? cdx + br_xmin : globals.g[i].br_xmin;
        br_xmax = cdx + br_xmax <= globals.g[i].br_xmax? cdx + br_xmax : globals.g[i].br_xmax;
        br_ymin = cdy + br_ymin >= globals.g[i].br_ymin? cdy + br_ymin : globals.g[i].br_ymin;
        br_ymax = cdy + br_ymax <= globals.g[i].br_ymax? cdy + br_ymax : globals.g[i].br_ymax;
        cdx = globals.g[i].dx;
        cdy = globals.g[i].dy;
        dx += cdx;
        dy += cdy;
        rd += globals.g[i].rel_depth;
        vis = vis * globals.g[i].desc_vis;
    }
    return recursive_info(dx, dy, rd, br_xmin, br_xmax, br_ymin, br_ymax, vis);
}
float relative_x(int x){
    float x_float = x;
    float window_width_float = _window_width;
    return 2*x_float/window_width_float -1;
}
float relative_y(int y){
    float y_float = y;
    float window_height_float = _window_height;
    return 2*y_float/window_height_float -1;
}

out VS_OUT{
    vec4 color;
    ivec2 size;
    ivec4 br;
    int vis;
} vs_out;

void main()
{
    recursive_info ri = calculate_recursives(id);

    int x = ri.x_trafo + position.x;
    int y = ri.y_trafo + position.y;
    vs_out.color = xcolor;
    vs_out.size = size;
    vs_out.br = ivec4(ri.br_xmin, ri.br_xmax, ri.br_ymin, ri.br_ymax);
    vs_out.vis = ri.vis;
    gl_Position = vec4(relative_x(x), relative_y(y), -ri.relative_depth/1000.0, 1.0);
}