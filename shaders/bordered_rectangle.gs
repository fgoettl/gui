#version 430 core
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform int _window_width;
uniform int _window_height;

in VS_OUT{
    vec4 color;
    ivec2 size;
    ivec4 br;
    int vis;
} gs_in[];

float relative_dx(int x){
    float x_float = x;
    float window_width_float = _window_width;
    return 2*x_float/window_width_float;
}
float relative_dy(int y){
    float y_float = y;
    float window_height_float = _window_height;
    return 2*y_float/window_height_float;
}

out vec4 color;
out vec2 tex_coord;
flat out ivec4 br;

void main() {
    if(gs_in[0].vis == 1){
        float dx = relative_dx(gs_in[0].size.x);
        float dy = relative_dy(gs_in[0].size.y);

        br = gs_in[0].br;
        color = gs_in[0].color;

        gl_Position = gl_in[0].gl_Position;
        tex_coord = vec2(0.0,0.0);
        EmitVertex();

        gl_Position = gl_in[0].gl_Position + vec4(dx, 0.0, 0.0, 0.0);
        tex_coord = vec2(1.0,0.0);
        EmitVertex();

        gl_Position = gl_in[0].gl_Position + vec4(0.0, dy, 0.0, 0.0);
        tex_coord = vec2(0.0,1.0);
        EmitVertex();

        gl_Position = gl_in[0].gl_Position + vec4(dx, dy, 0.0, 0.0);
        tex_coord = vec2(1.0,1.0);
        color = vec4(1.0,1.0,1.0,1.0);
        EmitVertex();
    }
    EndPrimitive();
}