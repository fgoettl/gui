#version 430 core
in vec4 color;
in vec2 tex_coord;
flat in ivec4 br;
out vec4 outColor;

layout (binding = 0) uniform sampler2D tex1;

void main()
{
    float x = gl_FragCoord.x;
    float y = gl_FragCoord.y;
    if(x < br.x || x > br.y || y < br.z || y > br.w) discard;
    outColor = color * texture(tex1, tex_coord);
}
