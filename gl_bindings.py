from __future__ import annotations
import weakref
import ctypes as C
from typing import NamedTuple

import pyglet.gl as GL

BufferObject = C.c_uint


class VertexArrayObject:
    _vao: C.c_uint

    def __init__(self):
        self._vao = C.c_uint()
        GL.glGenVertexArrays(1, C.byref(self._vao))
        weakref.finalize(self, delete_vao, self._vao)

    def __enter__(self):
        GL.glBindVertexArray(self._vao)

    def __exit__(self, type, value, traceback):
        GL.glBindVertexArray(0)


def delete_vao(vao: C.c_uint) -> None:
    GL.glDeleteVertexArrays(1, C.byref(vao))


class VertexBufferObject:
    _vbo: BufferObject
    _size: int
    _draw_type: GL.GLenum
    _c_type: type
    _gl_type: GL.GLenum

    def __init__(self, c_type: type = C.c_float, gl_type: GL.GLenum = GL.GL_FLOAT,
                 draw_type: GL.GLenum = GL.GL_STATIC_DRAW):
        self._vbo = BufferObject()
        GL.glGenBuffers(1, C.byref(self._vbo))
        self._size = -1
        self._draw_type = draw_type
        self._c_type = c_type
        self._gl_type = gl_type
        weakref.finalize(self, delete_buffer_object, self._vbo)

    def fill(self, data):
        c_data = (self._c_type * len(data))(*data)
        size: int = C.sizeof(c_data)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self._vbo)
        if size > self._size or size < self._size / 4:
            GL.glBufferData(GL.GL_ARRAY_BUFFER, size, C.byref(c_data), self._draw_type)
            self._size = size
        else:
            GL.glBufferSubData(GL.GL_ARRAY_BUFFER, 0, size, C.byref(c_data))

    def add_vertex_attribute_pointer(self, location: int, count_per_vertex: int, stride: int = -1, offset: int = 0):
        if stride == -1:
            stride = count_per_vertex
        stride_bytes: int = stride * C.sizeof(self._c_type())
        offset_bytes: int = offset * C.sizeof(self._c_type())
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self._vbo)
        if self._gl_type in (
                GL.GL_INT, GL.GL_UNSIGNED_INT, GL.GL_SHORT, GL.GL_UNSIGNED_SHORT, GL.GL_BYTE, GL.GL_UNSIGNED_BYTE):
            GL.glVertexAttribIPointer(location, count_per_vertex, self._gl_type, stride_bytes,
                                      C.c_void_p(offset_bytes))
        else:
            GL.glVertexAttribPointer(location, count_per_vertex, self._gl_type, GL.GL_FALSE, stride_bytes,
                                     C.c_void_p(offset_bytes))
        GL.glEnableVertexAttribArray(location)


def delete_buffer_object(buffer_object: BufferObject) -> None:
    GL.glDeleteBuffers(1, C.byref(buffer_object))


class ElementBufferObject:
    _ebo: BufferObject
    _size: int
    _draw_type: GL.GLenum

    def __init__(self, draw_type: GL.GLenum = GL.GL_STATIC_DRAW):
        self._ebo = BufferObject()
        GL.glGenBuffers(1, C.byref(self._ebo))
        self._size = -1
        self._draw_type = draw_type
        weakref.finalize(self, delete_buffer_object, self._ebo)

    def fill(self, indices):
        c_indices = (C.c_uint * len(indices))(*indices)
        size: int = C.sizeof(c_indices)
        GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, self._ebo)
        if size > self._size or size < self._size / 4:
            GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, size, C.byref(c_indices), self._draw_type)
            self._size = size
        else:
            GL.glBufferSubData(GL.GL_ELEMENT_ARRAY_BUFFER, 0, size, C.byref(c_indices))

    def bind(self):
        GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, self._ebo)


class ShaderStorageBufferObject:
    _ssbo: BufferObject
    _size: int
    _draw_type: GL.GLenum
    _c_type: type
    _gl_type: GL.GLenum

    def __init__(self, c_type: type = C.c_float, gl_type: GL.GLenum = GL.GL_FLOAT,
                 draw_type: GL.GLenum = GL.GL_STATIC_DRAW):
        self._ssbo = BufferObject()
        GL.glGenBuffers(1, C.byref(self._ssbo))
        self._size = -1
        self._draw_type = draw_type
        self._c_type = c_type
        self._gl_type = gl_type
        weakref.finalize(self, delete_buffer_object, self._ssbo)

    def fill(self, data):
        c_data = (self._c_type * len(data))(*data)
        size: int = C.sizeof(c_data)
        GL.glBindBuffer(GL.GL_SHADER_STORAGE_BUFFER, self._ssbo)
        if size > self._size or size < self._size / 4:
            GL.glBufferData(GL.GL_SHADER_STORAGE_BUFFER, size, C.byref(c_data), self._draw_type)
            self._size = size
        else:
            GL.glBufferSubData(GL.GL_SHADER_STORAGE_BUFFER, 0, size, C.byref(c_data))

    def update(self, data, offset: int):
        c_data = (self._c_type * len(data))(*data)
        type_size: int = C.sizeof(self._c_type())
        GL.glBindBuffer(GL.GL_SHADER_STORAGE_BUFFER, self._ssbo)
        GL.glBufferSubData(GL.GL_SHADER_STORAGE_BUFFER, offset * type_size, len(data) * type_size, C.byref(c_data))

    def copy_from(self, source: ShaderStorageBufferObject):
        if self._c_type != source._c_type:
            print("ERROR: Attempted copying data between Buffers of different types.")
            return
        min_size: int = min(self._size, source._size)
        GL.glBindBuffer(GL.GL_COPY_READ_BUFFER, source._ssbo)
        GL.glBindBuffer(GL.GL_COPY_WRITE_BUFFER, self._ssbo)
        GL.glCopyBufferSubData(GL.GL_COPY_READ_BUFFER, GL.GL_COPY_WRITE_BUFFER, 0, 0, min_size)

    def bind_to_binding_index(self, binding_index: int):
        GL.glBindBufferBase(GL.GL_SHADER_STORAGE_BUFFER, binding_index, self._ssbo)


class Texture2D:
    _texture: C.c_uint
    _width: int
    _height: int

    def __init__(self, width: int, height: int, data, internal_format: GL.GLint = GL.GL_RGBA,
                 format: GL.GLenum = GL.GL_RGBA, c_type: type = C.c_ubyte, gl_type: GL.GLenum = GL.GL_UNSIGNED_BYTE,
                 wrap_s: GL.GLint = GL.GL_CLAMP_TO_BORDER, wrap_t: GL.GLint = GL.GL_CLAMP_TO_BORDER,
                 min_filter: GL.GLint = GL.GL_LINEAR, mag_filter: GL.GLint = GL.GL_LINEAR):
        self._texture = C.c_uint()
        self._width = width
        self._height = height
        GL.glGenTextures(1, C.byref(self._texture))
        GL.glBindTexture(GL.GL_TEXTURE_2D, self._texture)
        c_data = (c_type * len(data))(*data)
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, wrap_s)
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, wrap_t)
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, min_filter)
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, mag_filter)
        GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, gl_type, c_data)
        GL.glGenerateMipmap(GL.GL_TEXTURE_2D)
        weakref.finalize(self, delete_texture, self._texture)

    def bind_to_texture_unit(self, unit: int) -> None:
        if not 0 <= unit <= 15:
            print("ERROR: Only allowed to use texture units 0 through 15.")
            return
        GL.glActiveTexture(GL.GL_TEXTURE0 + unit)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self._texture)


def delete_texture(texture: C.c_uint) -> None:
    GL.glDeleteTextures(1, C.byref(texture))
