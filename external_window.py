from typing import Callable

import pyglet
import pyglet.gl as GL

from rendering import AbsoluteRenderer
from ui_geometry import Point2i, Rectangle
from ui_rendering import UiObject, UiRenderer, GlobalDataFieldSpecification, RenderType
from user_input import MouseEvent
from color import Color


class ExternalWindowRoot(UiObject):
    def __init__(self, ui_renderer: UiRenderer):
        UiObject.__init__(self, RenderType.NON_RENDER)
        self._set_parent_data(self, ui_renderer)

    def _mouse_hit(self, mouse_event: MouseEvent) -> bool:
        return False

    def get_vertex_count(self) -> int:
        pass

    def get_static_data(self):
        pass

    def get_dynamic_data(self):
        pass

    def get_stream_data(self):
        pass

    def get_element_indices(self) -> list[int]:
        pass


class ExternalWindow(pyglet.window.Window):
    _window_width: int
    _window_height: int
    _window_background_color: Color
    _window_caption: str
    _status_bar: bool
    _root_ui_object: ExternalWindowRoot
    _ui_renderer: UiRenderer
    _mouse_pos: Point2i

    def __init__(self, width: int = 1000, height: int = 1000, caption: str = "", status_bar: bool = True,
                 background_color: Color = (0, 0, 0, 1)):
        self._window_width = width
        self._window_height = height
        self._window_background_color = background_color
        self._window_caption = caption
        self._status_bar = status_bar
        self._ui_renderer = UiRenderer([], [], width, height)
        self._root_ui_object = ExternalWindowRoot(self._ui_renderer)
        self._root_ui_object.bounding_rectangle = Rectangle(0, width, 0, height)
        self._mouse_pos = Point2i(-1, -1)
        self.schedule_interval(self._trigger_mouse_motion)
        self._initialize_window()

    def _initialize_window(self) -> None:
        style = pyglet.window.Window.WINDOW_STYLE_DEFAULT if self._status_bar else pyglet.window.Window.WINDOW_STYLE_BORDERLESS
        pyglet.window.Window.__init__(self, width=self._window_width, height=self._window_height,
                                      caption=self._window_caption,
                                      style=style)
        GL.glEnable(GL.GL_DEPTH_TEST)
        GL.glClearColor(*self._window_background_color)

    def add_global_data_fields(self, *gdfs: GlobalDataFieldSpecification) -> None:
        self._ui_renderer.add_global_data_fields(*gdfs)

    def add_renderer(self, render_type: RenderType, renderer: AbsoluteRenderer) -> None:
        self._ui_renderer.add_renderer(render_type, renderer)

    def add_child(self, ui_object: UiObject) -> None:
        self._root_ui_object.add_child(ui_object)

    def remove_child(self, ui_object: UiObject, remove_from_rendering: bool = True) -> None:
        self._root_ui_object.remove_child(ui_object, remove_from_rendering)

    def on_draw(self) -> None:
        self.clear()
        self._ui_renderer.render()

    def schedule_interval(self, func: Callable, interval: float = 1 / 60, *args, **kwargs) -> None:
        pyglet.clock.schedule_interval(func, interval, *args, **kwargs)

    def unschedule(self, func: Callable) -> None:
        pyglet.clock.unschedule(func)

    def _trigger_mouse_motion(self, dt):
        self._root_ui_object.handle_mouse_position(self._mouse_pos)

    def on_mouse_press(self, x, y, buttons, modifiers) -> None:
        self._root_ui_object.handle_mouse_event(MouseEvent(Point2i(x, y), buttons, modifiers, True))

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers) -> None:
        self._mouse_pos = Point2i(x, y)

    def on_mouse_motion(self, x, y, dx, dy) -> None:
        self._mouse_pos = Point2i(x, y)

    def on_mouse_release(self, x, y, buttons, modifiers) -> None:
        self._root_ui_object.handle_mouse_event(MouseEvent(Point2i(x, y), buttons, modifiers, False))
