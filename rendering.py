from __future__ import annotations
from typing import NamedTuple
import ctypes as C
import pyglet.gl as GL
from abc import ABC, abstractmethod

from gl_bindings import VertexBufferObject, VertexArrayObject, ElementBufferObject, ShaderStorageBufferObject
from shader_program import ShaderProgram


class DataSpecification(NamedTuple):  # TODO Namen überdenken
    name: str
    C_type: type
    GL_type: GL.GLenum
    count_per_vertex: int
    binding_location: int


class Renderable:
    _render_data_handler: RenderDataHandler | None
    id_from_render_data_handler: int

    def __init__(self):
        self._render_data_handler = None
        self.id_from_render_data_handler = 0

    def set_render_data_handler(self, render_data_handler: RenderDataHandler | None,
                                id_from_render_data_handler: int) -> None:
        self._render_data_handler = render_data_handler
        self.id_from_render_data_handler = id_from_render_data_handler

    def dynamic_data_changed(self) -> None:
        if self._render_data_handler is not None:
            self._render_data_handler.notify_about_change_of_dynamic_data(self.id_from_render_data_handler)

    def get_vertex_count(self) -> int:
        raise NotImplementedError

    def get_static_data(self):
        raise NotImplementedError

    def get_dynamic_data(self):
        raise NotImplementedError

    def get_stream_data(self):
        raise NotImplementedError

    def get_element_indices(self) -> list[int]:
        raise NotImplementedError


class RenderDataHandler(ABC):  # Takes care of updating the vbos end ebo of a renderer.
    _static_data_specification: list[DataSpecification]
    _dynamic_data_specification: list[DataSpecification]
    _stream_data_specification: list[DataSpecification]

    _static_vbos: list[VertexBufferObject]
    _dynamic_vbos: list[VertexBufferObject]
    _stream_vbos: list[VertexBufferObject]
    _ebo: ElementBufferObject

    def __init__(self, static_data_specs: list[DataSpecification], dynamic_data_specs: list[DataSpecification],
                 stream_data_specs: list[DataSpecification], static_vbos: list[VertexBufferObject],
                 dynamic_vbos: list[VertexBufferObject], stream_vbos: list[VertexBufferObject],
                 ebo: ElementBufferObject):
        self._static_data_specification = static_data_specs
        self._dynamic_data_specification = dynamic_data_specs
        self._stream_data_specification = stream_data_specs
        self._static_vbos = static_vbos
        self._dynamic_vbos = dynamic_vbos
        self._stream_vbos = stream_vbos
        self._ebo = ebo

    @abstractmethod
    def register_renderable(self, renderable: Renderable) -> None:
        raise NotImplementedError

    @abstractmethod
    def unregister_renderable(self, renderable: Renderable) -> None:
        raise NotImplementedError

    @abstractmethod
    def notify_about_change_of_dynamic_data(self, renderable_id: int, data_field_index: int = -1, data=None) -> None:
        raise NotImplementedError

    @abstractmethod
    def finalize_changes(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def get_element_count(self) -> int:
        raise NotImplementedError


class Renderer:
    _render_data_handler: RenderDataHandler
    _static_data_specification: list[DataSpecification]
    _dynamic_data_specification: list[DataSpecification]
    _stream_data_specification: list[DataSpecification]
    _shader_program: ShaderProgram
    _vao: VertexArrayObject
    _static_vbos: list[VertexBufferObject]
    _dynamic_vbos: list[VertexBufferObject]
    _stream_vbos: list[VertexBufferObject]
    _element_count: int
    _ebo: ElementBufferObject
    _primitive_type: GL.GLenum

    def __init__(self, static_data_specs: list[DataSpecification], dynamic_data_specs: list[DataSpecification],
                 stream_data_specs: list[DataSpecification],
                 shader_program: ShaderProgram,
                 render_data_handler_class: type,
                 primitive_type: GL.GLenum = GL.GL_TRIANGLES):
        self._static_data_specification = static_data_specs
        self._dynamic_data_specification = dynamic_data_specs
        self._stream_data_specification = stream_data_specs
        self._shader_program = shader_program
        self._primitive_type = primitive_type
        self._vao = VertexArrayObject()
        with self._vao:
            self._init_vbos()
            self._init_ebo()
        self._render_data_handler = render_data_handler_class(static_data_specs, dynamic_data_specs,
                                                              stream_data_specs, self._static_vbos, self._dynamic_vbos,
                                                              self._stream_vbos, self._ebo)

    def _init_vbos(self) -> None:
        self._static_vbos = [VertexBufferObject(specs.C_type, specs.GL_type, GL.GL_STATIC_DRAW) for specs in
                             self._static_data_specification]
        self._dynamic_vbos = [VertexBufferObject(specs.C_type, specs.GL_type, GL.GL_DYNAMIC_DRAW) for specs in
                              self._dynamic_data_specification]
        self._stream_vbos = [VertexBufferObject(specs.C_type, specs.GL_type, GL.GL_STREAM_DRAW) for specs in
                             self._stream_data_specification]
        for vbo, specs in zip(self._static_vbos, self._static_data_specification):
            vbo.add_vertex_attribute_pointer(specs.binding_location, specs.count_per_vertex)
        for vbo, specs in zip(self._dynamic_vbos, self._dynamic_data_specification):
            vbo.add_vertex_attribute_pointer(specs.binding_location, specs.count_per_vertex)
        for vbo, specs in zip(self._stream_vbos, self._stream_data_specification):
            vbo.add_vertex_attribute_pointer(specs.binding_location, specs.count_per_vertex)

    def _init_ebo(self) -> None:
        self._ebo = ElementBufferObject()
        self._ebo.bind()

    def render(self) -> None:
        self._render_data_handler.finalize_changes()
        element_count: int = self._render_data_handler.get_element_count()
        with self._shader_program:
            with self._vao:
                GL.glDrawElements(self._primitive_type, element_count, GL.GL_UNSIGNED_INT, C.c_void_p(0))

    def register_renderable(self, renderable: Renderable) -> None:
        self._render_data_handler.register_renderable(renderable)

    def unregister_renderable(self, renderable: Renderable) -> None:
        self._render_data_handler.unregister_renderable(renderable)

    def set_uniform_bool(self, name: str, value: bool) -> None:
        with self._shader_program:
            self._shader_program.set_uniform_bool(name, value)

    def set_uniform_int(self, name: str, value: int) -> None:
        with self._shader_program:
            self._shader_program.set_uniform_int(name, value)

    def set_uniform_float(self, name: str, value: float) -> None:
        with self._shader_program:
            self._shader_program.set_uniform_float(name, value)

    def set_uniform_float4(self, name: str, value1: float, value2: float, value3: float, value4: float) -> None:
        with self._shader_program:
            self._shader_program.set_uniform_float4(name, value1, value2, value3, value4)

    def set_uniform_mat4(self, name: str, matrix) -> None:
        with self._shader_program:
            self._shader_program.set_uniform_mat4(name, matrix)

    def bind_ssbo(self, ssbo: ShaderStorageBufferObject, binding_location: int) -> None:
        with self._vao:
            ssbo.bind_to_binding_index(binding_location)


class AbsoluteRenderer(Renderer):
    _window_width: int
    _window_height: int

    def set_window_dimensions(self, window_width: int, window_height: int) -> None:
        self.set_uniform_int("_window_width", window_width)
        self.set_uniform_int("_window_height", window_height)


class UiElementRenderer(AbsoluteRenderer):
    def __init__(self, static_data_specs: list[DataSpecification], dynamic_data_specs: list[DataSpecification],
                 stream_data_specs: list[DataSpecification],
                 shader_program: ShaderProgram,
                 render_data_handler_class: type,
                 primitive_type: GL.GLenum = GL.GL_TRIANGLES):
        AbsoluteRenderer.__init__(self, static_data_specs + [DataSpecification("id", C.c_int, GL.GL_INT, 1, 0)],
                                  dynamic_data_specs, stream_data_specs, shader_program,
                                  render_data_handler_class, primitive_type)
