from __future__ import annotations
from enum import Enum
from typing import NamedTuple

from ui_geometry import Transformation, Point2i

MouseButton = int


class MouseEvent(NamedTuple):
    pos: Point2i
    button: MouseButton
    modifier: int
    pressed: bool  # or released


class EventHandleStatus(Enum):
    HANDLED = 1
    NOT_HANDLED = 0


def transform_mouse_event(trafo: Transformation, mouse_event: MouseEvent) -> MouseEvent:
    return MouseEvent(trafo(mouse_event.pos), mouse_event.button, mouse_event.modifier, mouse_event.pressed)

