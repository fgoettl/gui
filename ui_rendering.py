from __future__ import annotations

from abc import abstractmethod, ABC
from enum import Enum
from typing import NamedTuple
from collections.abc import Iterable
import ctypes as C
import pyglet.gl as GL

from gl_bindings import ShaderStorageBufferObject
from renderers import RenderType, create_renderer
from rendering import Renderable, AbsoluteRenderer
from ui_geometry import Transformation, Rectangle, Point2i
from user_input import MouseEvent, transform_mouse_event, EventHandleStatus, MouseButton


class IDHandler:
    _free_ids: list[int]
    _next_new_id: int

    def __init__(self):
        self._free_ids = []
        self._next_new_id = 0

    def get_free_id(self) -> int:
        if self._free_ids:
            return self._free_ids.pop()
        else:
            self._next_new_id += 1
            return self._next_new_id - 1

    def free_id(self, _id: int) -> None:
        self._free_ids.append(_id)


class GlobalDataFieldSpecification(NamedTuple):
    name: str
    C_type: type
    GL_type: GL.GLenum
    count_per_ui_object: int
    binding: int
    default: int | float


_BUILTIN_GLOBALS_NAME = "_builtin_globals"


def _check_and_add_builtin_globals_data_field(
        global_data_field_specifications: list[GlobalDataFieldSpecification]) -> None:
    specified_bindings: set[int] = {specs.binding for specs in global_data_field_specifications}
    specified_gdf_names: set[str] = {specs.name for specs in global_data_field_specifications}
    if _BUILTIN_GLOBALS_NAME in specified_gdf_names:
        pass
    elif 0 not in specified_bindings:
        global_data_field_specifications.append(
            GlobalDataFieldSpecification(_BUILTIN_GLOBALS_NAME, C.c_int, GL.GL_INT, 9, 0, 0))
    else:
        print("ERROR: Attempt to override binding slot of builtin global data field.")


class UiRenderer:
    _renderers: list[AbsoluteRenderer]
    _renderers_dict: dict[RenderType, AbsoluteRenderer]

    _global_data_fields_specifications: list[GlobalDataFieldSpecification]
    _global_data_fields_dict: dict[str, int]
    _global_data_fields_ssbos: list[ShaderStorageBufferObject]
    _global_data_field_max: int
    _INITIAL_MAX = 16
    _FACTOR_MAX = 2

    _window_width: int
    _window_height: int

    _id_handler: IDHandler

    def __init__(self, elementary_renderers: list[(RenderType, AbsoluteRenderer)],
                 global_data_fields: list[GlobalDataFieldSpecification],
                 window_width: int, window_height: int):
        self._renderers = [r for _, r in elementary_renderers]
        self._renderers_dict = {n: r for n, r in elementary_renderers}

        self._init_global_data_fields(global_data_fields)
        self.set_window_dimensions(window_width, window_height)

        self._id_handler = IDHandler()

    def _init_global_data_fields(self, global_data_field_specifications: list[GlobalDataFieldSpecification]) -> None:
        _check_and_add_builtin_globals_data_field(global_data_field_specifications)
        self._global_data_fields_dict = dict()
        self._global_data_fields_specifications = []
        self._global_data_fields_ssbos = []
        self._global_data_field_max = UiRenderer._INITIAL_MAX
        for gdfs in global_data_field_specifications:
            self._create_and_init_ssbo(gdfs)
        self._bind_ssbos_to_renderers()

    def _create_and_init_ssbo(self, gdfs: GlobalDataFieldSpecification) -> None:
        new_index: int = len(self._global_data_fields_specifications)
        self._global_data_fields_dict[gdfs.name] = new_index
        self._global_data_fields_specifications.append(gdfs)
        ssbo: ShaderStorageBufferObject = ShaderStorageBufferObject(gdfs.C_type, gdfs.GL_type, GL.GL_DYNAMIC_DRAW)
        self._global_data_fields_ssbos.append(ssbo)
        initial_data = [gdfs.default] * (self._global_data_field_max * gdfs.count_per_ui_object)
        ssbo.fill(initial_data)

    def _bind_ssbos_to_renderers(self) -> None:
        for gdfs, ssbo in zip(self._global_data_fields_specifications, self._global_data_fields_ssbos):
            for renderer in self._renderers:
                renderer.bind_ssbo(ssbo, gdfs.binding)

    def _extend_global_data_fields(self) -> None:
        old_gdf_max: int = self._global_data_field_max
        new_gdf_max: int = old_gdf_max * UiRenderer._FACTOR_MAX
        for i, gdfs in enumerate(self._global_data_fields_specifications):
            old_ssbo: ShaderStorageBufferObject = self._global_data_fields_ssbos[i]
            new_ssbo: ShaderStorageBufferObject = ShaderStorageBufferObject(gdfs.C_type, gdfs.GL_type,
                                                                            GL.GL_DYNAMIC_DRAW)
            default_data = [gdfs.default] * (new_gdf_max * gdfs.count_per_ui_object)
            new_ssbo.fill(default_data)
            new_ssbo.copy_from(old_ssbo)
            self._global_data_fields_ssbos[i] = new_ssbo
        self._global_data_field_max = new_gdf_max
        self._bind_ssbos_to_renderers()

    def set_window_dimensions(self, width: int, height: int) -> None:
        self._window_width = width
        self._window_height = height
        for renderer in self._renderers:
            renderer.set_window_dimensions(width, height)

    def get_global_position(self, gdf_name: str) -> int:
        return self._global_data_fields_dict[gdf_name]

    def update_global_by_name(self, gdf_name: str, uiobject_id: int, data) -> None:
        self.update_global(self.get_global_position(gdf_name), uiobject_id, data)

    def update_global(self, gdf_index: int, uiobject_id: int, data) -> None:
        ssbo: ShaderStorageBufferObject = self._global_data_fields_ssbos[gdf_index]
        offset: int = uiobject_id * self._global_data_fields_specifications[gdf_index].count_per_ui_object
        ssbo.update(data, offset)

    def add_renderer(self, render_type: RenderType, renderer: AbsoluteRenderer) -> None:
        if render_type in self._renderers_dict:
            print("ERROR: UiElementaryRenderer with Type ", render_type, " already present.")
            return
        self._renderers.append(renderer)
        renderer.set_window_dimensions(self._window_width, self._window_height)
        self._renderers_dict[render_type] = renderer
        for gdfs, ssbo in zip(self._global_data_fields_specifications, self._global_data_fields_ssbos):
            renderer.bind_ssbo(ssbo, gdfs.binding)

    def add_global_data_fields(self, *gdfs: GlobalDataFieldSpecification) -> None:
        for spec in gdfs:
            self._create_and_init_ssbo(spec)
        self._bind_ssbos_to_renderers()

    def register_ui_object(self, uio: UiObject) -> int:
        render_type: RenderType = uio.render_type
        if render_type not in self._renderers_dict:
            self.add_renderer(render_type, create_renderer(render_type, self._window_width, self._window_height))

        new_id: int = self._id_handler.get_free_id()
        if new_id >= self._global_data_field_max:
            self._extend_global_data_fields()
        el_renderer: AbsoluteRenderer = self._renderers_dict[render_type]
        el_renderer.register_renderable(uio)
        return new_id

    def unregister_ui_object(self, uio: UiObject, id_: int) -> None:
        el_renderer: AbsoluteRenderer = self._renderers_dict[uio.render_type]
        el_renderer.unregister_renderable(uio)
        self._id_handler.free_id(id_)

    def render(self) -> None:
        for renderer in self._renderers:
            renderer.render()


class MouseSettings:
    clickable: bool
    mouse_over_enabled: bool
    blocking: bool

    mouse_over_needed: bool
    clicks_needed: bool

    def __init__(self, clickable: bool = False, mouse_over_enabled: bool = False,
                 blocking: bool = True):
        self.clickable = clickable
        self.mouse_over_enabled = mouse_over_enabled
        self.blocking = blocking
        self.mouse_over_needed = self.clickable or self.mouse_over_enabled or self.blocking
        self.clicks_needed = self.clickable or self.blocking

    def __iter__(self):
        return iter((self.clickable, self.mouse_over_enabled, self.blocking))


class UiObject(Renderable):
    _id_from_ui_renderer: int
    _ui_renderer: UiRenderer | None
    _render_type: RenderType

    _depth_manager: DepthManager
    _parent: UiObject
    _self_to_parent_transformation: Transformation
    _relative_depth: int
    _bounding_rectangle: Rectangle
    _descendant_visibility: int

    _builtin_globals_index: int

    _mouse_settings: MouseSettings
    _descendant_needs_click: bool
    _true_desc_that_need_clicks: int
    _mouse_over_since_last_button_down: list[MouseButton]
    _descendant_needs_mouse_over: bool
    _true_desc_that_need_mouse_over: int
    _mouse_over: bool
    _true_desc_mouse_over: bool
    _mouse_position: Point2i

    def __init__(self, render_type: RenderType = RenderType.NON_RENDER, mouse_settings: MouseSettings | None = None,
                 depth_manager_type: type | None = None):
        Renderable.__init__(self)
        self._id_from_ui_renderer = -1
        self._ui_renderer = None
        self._render_type = render_type

        self._depth_manager = depth_manager_type(self) if depth_manager_type is not None else SimpleDepthManager(self)
        self._parent = self
        self._self_to_parent_transformation = Transformation(0, 0)
        self._relative_depth = 0
        self._bounding_rectangle = Rectangle(0, 0, 0, 0)
        self._descendant_visibility = 1

        self._builtin_globals_index = -1

        self._mouse_settings = mouse_settings if mouse_settings is not None else MouseSettings()
        self._descendant_needs_click = self._mouse_settings.clicks_needed
        self._true_desc_that_need_clicks = 0
        self._mouse_over_since_last_button_down = []
        self._descendant_needs_mouse_over = self._mouse_settings.mouse_over_needed
        self._true_desc_that_need_mouse_over = 0
        self._mouse_over = False
        self._true_desc_mouse_over = False
        self._mouse_position = Point2i(0, 0)

    def _set_ui_renderer(self, ui_renderer: UiRenderer) -> None:
        if ui_renderer == self._ui_renderer:
            return
        if self._ui_renderer is not None:
            self._unset_ui_renderer()
        id_from_ui_renderer: int = ui_renderer.register_ui_object(self)
        self._id_from_ui_renderer = id_from_ui_renderer
        self._ui_renderer = ui_renderer
        self._pull_builtin_globals_index()
        self._push_builtin_globals()
        for child in self.children:
            child._set_ui_renderer(ui_renderer)

    @property
    def descendant_needs_click(self) -> bool:
        return self._descendant_needs_click

    @descendant_needs_click.setter
    def descendant_needs_click(self, b: bool) -> None:
        before: bool = self._descendant_needs_click
        self._descendant_needs_click = b
        if self._parent == self:
            return
        if b and not before:
            self._parent.true_desc_that_need_clicks += 1
            self.depth_manager.mouse_needs_changed()
        if before and not b:
            self._parent.true_desc_that_need_clicks -= 1
            self.depth_manager.mouse_needs_changed()

    @property
    def true_desc_that_need_clicks(self) -> int:
        return self._true_desc_that_need_clicks

    @true_desc_that_need_clicks.setter
    def true_desc_that_need_clicks(self, k: int) -> None:
        self._true_desc_that_need_clicks = k
        self.descendant_needs_click = self._mouse_settings.clicks_needed or k > 0

    @property
    def mouse_settings(self) -> MouseSettings:
        return self._mouse_settings

    @mouse_settings.setter
    def mouse_settings(self, settings: MouseSettings) -> None:
        self._mouse_settings = settings
        self.descendant_needs_click = self._mouse_settings.clicks_needed or self._true_desc_that_need_clicks > 0
        self.descendant_needs_mouse_over = self._mouse_settings.mouse_over_needed or self._true_desc_that_need_mouse_over > 0

    @property
    def descendant_needs_mouse_over(self) -> bool:
        return self._descendant_needs_mouse_over

    @descendant_needs_mouse_over.setter
    def descendant_needs_mouse_over(self, b: bool) -> None:
        before: bool = self._descendant_needs_mouse_over
        self._descendant_needs_mouse_over = b
        if self._parent == self:
            return
        if b and not before:
            self._parent.true_desc_that_need_mouse_over += 1
            self.depth_manager.mouse_needs_changed()
        if before and not b:
            self._parent.true_desc_that_need_mouse_over -= 1
            self.depth_manager.mouse_needs_changed()

    @property
    def true_desc_that_need_mouse_over(self) -> int:
        return self._true_desc_that_need_mouse_over

    @true_desc_that_need_mouse_over.setter
    def true_desc_that_need_mouse_over(self, k: int) -> None:
        self._true_desc_that_need_mouse_over = k
        self.descendant_needs_mouse_over = self._mouse_settings.mouse_over_needed or k > 0

    def _pull_builtin_globals_index(self) -> None:
        if self._ui_renderer is None:
            return
        self._builtin_globals_index = self._ui_renderer.get_global_position(_BUILTIN_GLOBALS_NAME)

    def _push_builtin_globals(self) -> None:
        if self._ui_renderer is None:
            return
        self._ui_renderer.update_global(self._builtin_globals_index, self._id_from_ui_renderer,
                                        [self.parent._id_from_ui_renderer, *self._self_to_parent_transformation,
                                         self._relative_depth, *self._bounding_rectangle, self._descendant_visibility])

    def _clear_ui_renderer(self):
        if self._ui_renderer is not None:
            self._ui_renderer.unregister_ui_object(self, self._id_from_ui_renderer)
            self._ui_renderer = None
            self._id_from_ui_renderer = -1

    def _unset_ui_renderer(self) -> None:
        self._clear_ui_renderer()
        for child in self.children:
            child._unset_ui_renderer()

    def _set_parent_data(self, parent: UiObject, ui_renderer: UiRenderer | None) -> None:
        self._parent = parent
        self._set_ui_renderer(ui_renderer)

    def _unset_parent_data(self, remove_from_rendering: bool) -> None:
        self._parent = self
        if remove_from_rendering:
            self._unset_ui_renderer()
        else:
            self._push_builtin_globals()

    def add_child(self, child: UiObject, **kwargs) -> None:
        if child.descendant_needs_click:
            self.true_desc_that_need_clicks += 1
        if child.descendant_needs_mouse_over:
            self.true_desc_that_need_mouse_over += 1
        self._depth_manager.register_child(child, **kwargs)
        child._set_parent_data(self, self._ui_renderer)
        if child._mouse_over or child._true_desc_mouse_over:
            self.propagate_mouse_over_to_root()

    def propagate_mouse_over_to_root(self) -> None:
        if not self._true_desc_mouse_over:
            self._true_desc_mouse_over = True
            self._parent.propagate_mouse_over_to_root()

    def remove_child(self, child: UiObject, remove_from_rendering: bool = True) -> None:
        if child not in self.children:
            return
        if child.descendant_needs_click:
            self.true_desc_that_need_clicks -= 1
        if child.descendant_needs_mouse_over:
            self.true_desc_that_need_mouse_over -= 1
        self._depth_manager.unregister_child(child)
        child._unset_parent_data(remove_from_rendering)

    @property
    def parent(self) -> UiObject:
        return self._parent

    @property
    def transformation(self) -> Transformation:
        return self._self_to_parent_transformation

    @transformation.setter
    def transformation(self, t: Transformation) -> None:
        self._self_to_parent_transformation = t
        self._push_builtin_globals()

    @property
    def relative_depth(self) -> int:
        return self._relative_depth

    @relative_depth.setter
    def relative_depth(self, d: int) -> None:
        self._relative_depth = d
        self._push_builtin_globals()

    @property
    def bounding_rectangle(self) -> Rectangle:
        return self._bounding_rectangle

    @bounding_rectangle.setter
    def bounding_rectangle(self, r: Rectangle):
        self._bounding_rectangle = r
        self._push_builtin_globals()

    @property
    def descendant_visibility(self) -> bool:
        return self._descendant_visibility == 1

    @descendant_visibility.setter
    def descendant_visibility(self, visible: bool) -> None:
        self._descendant_visibility = 1 if visible else 0
        self._push_builtin_globals()

    @property
    def render_type(self) -> RenderType:
        return self._render_type

    def get_vertex_count(self) -> int:
        raise NotImplementedError

    def get_static_data(self):
        return self.get_static_data_without_id() + [[self._id_from_ui_renderer] * self.get_vertex_count()]

    def get_static_data_without_id(self):
        raise NotImplementedError

    def get_dynamic_data(self):
        raise NotImplementedError

    def get_stream_data(self):
        raise NotImplementedError

    def get_element_indices(self) -> list[int]:
        raise NotImplementedError

    @property
    def depth_manager(self) -> DepthManager:
        return self._depth_manager

    @property
    def _click_needing_children(self) -> Iterable[UiObject]:
        return self._depth_manager.get_click_needing_children()

    @property
    def _mouse_over_needing_children(self) -> Iterable[UiObject]:
        return self._depth_manager.get_mouse_over_needing_children()

    @property
    def children(self) -> Iterable[UiObject]:
        return self._depth_manager.get_children()

    def handle_mouse_position(self, mouse_position: Point2i | None) -> EventHandleStatus:
        if not self.descendant_needs_mouse_over:
            return EventHandleStatus.NOT_HANDLED
        if not self._mouse_over and not self._true_desc_mouse_over and mouse_position is None:
            return EventHandleStatus.NOT_HANDLED
        if mouse_position is not None:
            mouse_position = (~self._self_to_parent_transformation)(mouse_position)
        desc_handled: EventHandleStatus = self._handle_mouse_position_children(mouse_position)
        if desc_handled == EventHandleStatus.HANDLED:
            mouse_position = None
        self_handled: EventHandleStatus = self._handle_mouse_position_self(mouse_position)
        if desc_handled == EventHandleStatus.HANDLED or self_handled == EventHandleStatus.HANDLED:
            return EventHandleStatus.HANDLED
        return EventHandleStatus.NOT_HANDLED

    def _handle_mouse_position_children(self, mouse_position: Point2i | None) -> EventHandleStatus:
        if mouse_position is not None and mouse_position not in self._bounding_rectangle:
            mouse_position = None
        if mouse_position is None and not self._true_desc_mouse_over:
            return EventHandleStatus.NOT_HANDLED
        handled: bool = False
        for child in self._mouse_over_needing_children:
            if child.handle_mouse_position(mouse_position) == EventHandleStatus.HANDLED:
                handled = True
                mouse_position = None
        self._true_desc_mouse_over = any(
            [child._mouse_over or child._true_desc_mouse_over for child in self._mouse_over_needing_children])
        if handled:
            return EventHandleStatus.HANDLED
        else:
            return EventHandleStatus.NOT_HANDLED

    def _handle_mouse_position_self(self, mouse_position: Point2i | None) -> EventHandleStatus:
        if mouse_position is None or not self._mouse_hit(mouse_position):
            if not self._mouse_over:
                return EventHandleStatus.NOT_HANDLED
            else:
                self._mouse_over = False
                if self._mouse_settings.mouse_over_enabled:
                    self.on_mouse_exit()
                self._mouse_over_since_last_button_down = []
                return EventHandleStatus.NOT_HANDLED
        else:
            self._mouse_position = mouse_position
            if self._mouse_over and self._mouse_settings.mouse_over_enabled:
                self.on_mouse_over(mouse_position)
            else:
                self._mouse_over = True
                if self._mouse_settings.mouse_over_enabled:
                    self.on_mouse_enter()
            return EventHandleStatus.HANDLED if self._mouse_settings.blocking else EventHandleStatus.NOT_HANDLED

    def handle_mouse_event(self, mouse_event: MouseEvent) -> EventHandleStatus:
        if not self.descendant_needs_click:
            return EventHandleStatus.NOT_HANDLED
        me: MouseEvent = transform_mouse_event(~self._self_to_parent_transformation, mouse_event)
        if self._true_desc_that_need_clicks > 0 and me.pos in self._bounding_rectangle:
            for child in self._click_needing_children:
                if child.handle_mouse_event(me) == EventHandleStatus.HANDLED:
                    return EventHandleStatus.HANDLED
        if self._mouse_settings.clickable or self._mouse_settings.blocking:
            hit: bool = self._mouse_hit(me.pos)
            if self._mouse_settings.clickable and hit:
                self._on_mouse_event(me)
                return EventHandleStatus.HANDLED
            if self._mouse_settings.blocking and hit:
                return EventHandleStatus.HANDLED
        return EventHandleStatus.NOT_HANDLED

    def _mouse_hit(self, mouse_position: Point2i) -> bool:
        raise NotImplementedError

    def on_mouse_button_down(self, mouse_event: MouseEvent) -> None:
        raise NotImplementedError

    def on_mouse_button_up(self, mouse_event: MouseEvent) -> None:
        raise NotImplementedError

    def on_mouse_click(self, mouse_event: MouseEvent) -> None:
        raise NotImplementedError

    def on_mouse_enter(self) -> None:
        raise NotImplementedError

    def on_mouse_exit(self) -> None:
        raise NotImplementedError

    def on_mouse_over(self, mouse_position: Point2i) -> None:
        raise NotImplementedError

    def _on_mouse_button_down(self, mouse_event: MouseEvent) -> None:
        if mouse_event.button not in self._mouse_over_since_last_button_down:
            self._mouse_over_since_last_button_down.append(mouse_event.button)
        self.on_mouse_button_down(mouse_event)

    def _on_mouse_button_up(self, mouse_event: MouseEvent) -> None:
        self.on_mouse_button_up(mouse_event)
        if mouse_event.button in self._mouse_over_since_last_button_down:
            self.on_mouse_click(mouse_event)

    def _on_mouse_event(self, mouse_event: MouseEvent) -> None:
        if mouse_event.pressed:
            self._on_mouse_button_down(mouse_event)
        else:
            self._on_mouse_button_up(mouse_event)

    @property
    def clickable(self) -> bool:
        return self.mouse_settings.clickable

    @clickable.setter
    def clickable(self, c: bool) -> None:
        _, m, b = self.mouse_settings
        self.mouse_settings = MouseSettings(c, m, b)

    @property
    def mouse_over_enabled(self) -> bool:
        return self.mouse_settings.mouse_over_enabled

    @mouse_over_enabled.setter
    def mouse_over_enabled(self, m: bool) -> None:
        c, _, b = self.mouse_settings
        self.mouse_settings = MouseSettings(c, m, b)

    @property
    def blocking(self) -> bool:
        return self.mouse_settings.blocking

    @blocking.setter
    def blocking(self, b: bool) -> None:
        c, m, _ = self.mouse_settings
        self.mouse_settings = MouseSettings(c, m, b)


class DepthRangeSizeWishes(NamedTuple):
    wanted: int
    needed: int

    def __add__(self, other: DepthRangeSizeWishes) -> DepthRangeSizeWishes:
        return DepthRangeSizeWishes(self.wanted + other.wanted, self.needed + other.needed)


def wishes_sum(wishes: Iterable[DepthRangeSizeWishes]) -> DepthRangeSizeWishes:
    want_sum: int = 0
    need_sum: int = 0
    for w, n in wishes:
        want_sum += w
        need_sum += n
    return DepthRangeSizeWishes(want_sum, need_sum)


def wishes_max(wishes: Iterable[DepthRangeSizeWishes]) -> DepthRangeSizeWishes:
    want_max: int = 0
    need_max: int = 0
    for w, n in wishes:
        want_max = want_max if want_max >= w else w
        need_max = need_max if need_max >= n else n
    return DepthRangeSizeWishes(want_max, need_max)


class DepthRange(NamedTuple):
    size: int
    offset: int


class DepthManager(ABC):
    _parent_dm: DepthManager | None
    _ui_object: UiObject
    _downward_pass_from_parent_mandatory: bool
    _downward_pass_mandatory: bool

    def __init__(self, ui_object: UiObject):
        self._ui_object = ui_object
        self._parent_dm = None
        self._downward_pass_from_parent_mandatory = False
        self._downward_pass_mandatory = False

    ################################## EXTERN
    @abstractmethod
    def get_children(self) -> Iterable[UiObject]:
        raise NotImplementedError

    @abstractmethod
    def get_click_needing_children(self) -> Iterable[UiObject]:
        raise NotImplementedError

    @abstractmethod
    def get_mouse_over_needing_children(self) -> Iterable[UiObject]:
        raise NotImplementedError

    @abstractmethod
    def register_child(self, child: UiObject, **kwargs) -> None:
        raise NotImplementedError

    @abstractmethod
    def unregister_child(self, child: UiObject) -> None:
        raise NotImplementedError

    @abstractmethod
    def children_mouse_needs_changed(self, child: DepthManager):
        raise NotImplementedError

    @abstractmethod
    def update_child_depth(self, child: UiObject, **kwargs) -> None:
        raise NotImplementedError

    ######################################

    ###################################### Intern
    def mouse_needs_changed(self) -> None:
        if self._parent_dm is not None:
            self._parent_dm.children_mouse_needs_changed(self)

    @abstractmethod
    def get_wishes(self) -> DepthRangeSizeWishes:
        raise NotImplementedError

    @abstractmethod
    def on_upward_pass(self, child: DepthManager, child_needs_downward_pass: bool) -> bool:
        # Returns whether the upward_pass should continue or downward_passes should be started
        raise NotImplementedError

    @abstractmethod
    def start_downward_passes(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def on_downward_pass(self, depth_range: DepthRange) -> bool:
        # Returns whether the downward_pass should continue or not
        raise NotImplementedError

    @abstractmethod
    def on_downward_pass_root(self) -> bool:
        # Returns whether the downward_pass should continue or not
        raise NotImplementedError

    @abstractmethod
    def on_fresh_downward_pass(self) -> bool:
        # Returns whether the downward_pass should continue or not
        raise NotImplementedError

    def need_downward_pass(self) -> None:
        self._downward_pass_mandatory = True

    def need_downward_pass_from_parent(self) -> None:
        self._downward_pass_from_parent_mandatory = True
        self.need_downward_pass()

    def upward_pass(self, child: DepthManager, child_needs_downward_pass: bool) -> None:
        if self.on_upward_pass(child, child_needs_downward_pass):
            if self._parent_dm is None:
                print("ERROR: Can't continue upward_pass beyond root")
            self._parent_dm.upward_pass(self, self._downward_pass_from_parent_mandatory)
            if self._downward_pass_from_parent_mandatory:
                print("ERROR: Downward_pass from parent needed, but not received")
            if self._downward_pass_mandatory:
                self.downward_pass(None)
        else:
            self.downward_pass(None)

    def downward_pass(self, depth_range: DepthRange | None) -> None:
        if self._downward_pass_from_parent_mandatory and depth_range is None:
            print("ERROR: Downward_pass from parent needed, but not received")
        self._downward_pass_mandatory = False
        self._downward_pass_from_parent_mandatory = False
        start_passes: bool
        if depth_range is None:
            if self._parent_dm is None:
                start_passes = self.on_downward_pass_root()
            else:
                start_passes = self.on_fresh_downward_pass()
        else:
            start_passes = self.on_downward_pass(depth_range)
        if start_passes:
            self.start_downward_passes()

    def set_rel_depth(self, rel_depth: int) -> None:
        self._ui_object.relative_depth = rel_depth

    @property
    def parent_dm(self) -> DepthManager:
        return self._parent_dm

    @parent_dm.setter
    def parent_dm(self, dm: DepthManager) -> None:
        self._parent_dm = dm

    @property
    def ui_object(self) -> UiObject:
        return self._ui_object


class ChildDepthUpdatePosition(Enum):
    ON_TOP = 1
    ON_BOT = 2
    BELOW_X = 3
    ABOVE_X = 4
    ARBITRARY = 5
    COMPONENT_ON_TOP = 6
    COMPONENT_ON_BOT = 7


class SimpleDepthManager(DepthManager):
    ROOT_RANGE = DepthRange(2147483647, 0)
    _range: DepthRange

    _top_list: list[DepthManager]
    _between_list: list[DepthManager]
    _bot_list: list[DepthManager]
    _component_list: list[DepthManager]

    _wishes: DepthRangeSizeWishes
    _top_wishes: DepthRangeSizeWishes
    _between_wishes: DepthRangeSizeWishes
    _bot_wishes: DepthRangeSizeWishes
    _component_wishes: DepthRangeSizeWishes

    _children_need_downward_pass: bool

    _click_needing_children: list[UiObject]
    _mouse_over_needing_children: list[UiObject]

    def __init__(self, ui_object: UiObject):
        DepthManager.__init__(self, ui_object)
        self._top_list = []
        self._between_list = []
        self._bot_list = []
        self._component_list = []
        self._children_need_downward_pass = False
        self.recalculate_wishes()
        self._range = SimpleDepthManager.ROOT_RANGE
        self._click_needing_children = []
        self._mouse_over_needing_children = []

    def recalculate_wishes(self) -> None:
        self._top_wishes = wishes_sum(c.get_wishes() for c in self._top_list)
        self._between_wishes = wishes_max(c.get_wishes() for c in self._between_list)
        self._bot_wishes = wishes_sum(c.get_wishes() for c in self._bot_list)
        self._component_wishes = wishes_sum(c.get_wishes() for c in self._component_list)
        self._wishes = self._top_wishes + self._between_wishes + self._bot_wishes + self._component_wishes + DepthRangeSizeWishes(
            1, 1)

    def get_wishes(self) -> DepthRangeSizeWishes:
        return self._wishes

    def on_upward_pass(self, child: DepthManager, child_needs_downward_pass: bool) -> bool:
        if child_needs_downward_pass:
            self.need_downward_pass()
            self._children_need_downward_pass = True
        self.recalculate_wishes()
        if self._wishes.needed > self._range.size:
            self.need_downward_pass_from_parent()
            return True
        if self._wishes.wanted > self._range.size:
            self.need_downward_pass()
            return True
        return False

    def on_downward_pass(self, depth_range: DepthRange) -> bool:
        size_shrunk: bool = depth_range.size < self._range.size
        self._range = depth_range
        self.set_rel_depth(depth_range.offset)
        return size_shrunk or self._children_need_downward_pass

    def on_downward_pass_root(self) -> bool:
        depth_range: DepthRange = SimpleDepthManager.ROOT_RANGE
        return self.on_downward_pass(depth_range)

    def on_fresh_downward_pass(self) -> bool:
        return self._children_need_downward_pass

    def start_downward_passes(self) -> None:
        current_offset: int = 1
        child_range_size: int
        if self._range.size >= self._wishes.wanted:
            for child_dm in self._component_list:
                child_range_size = child_dm.get_wishes().wanted
                child_dm.downward_pass(DepthRange(child_range_size, current_offset))
                current_offset += child_range_size
            for child_dm in reversed(self._bot_list):
                child_range_size = child_dm.get_wishes().wanted
                child_dm.downward_pass(DepthRange(child_range_size, current_offset))
                current_offset += child_range_size
            for child_dm in self._between_list:
                child_range_size = child_dm.get_wishes().wanted
                child_dm.downward_pass(DepthRange(child_range_size, current_offset))
            current_offset += self._between_wishes.wanted
            for child_dm in self._top_list:
                child_range_size = child_dm.get_wishes().wanted
                child_dm.downward_pass(DepthRange(child_range_size, current_offset))
                current_offset += child_range_size
        elif self._range.size >= self._wishes.needed:
            for child_dm in self._component_list:
                child_range_size = child_dm.get_wishes().needed
                child_dm.downward_pass(DepthRange(child_range_size, current_offset))
                current_offset += child_range_size
            for child_dm in reversed(self._bot_list):
                child_range_size = child_dm.get_wishes().needed
                child_dm.downward_pass(DepthRange(child_range_size, current_offset))
                current_offset += child_range_size
            for child_dm in self._between_list:
                child_range_size = child_dm.get_wishes().needed
                child_dm.downward_pass(DepthRange(child_range_size, current_offset))
            current_offset += self._between_wishes.needed
            for child_dm in self._top_list:
                child_range_size = child_dm.get_wishes().needed
                child_dm.downward_pass(DepthRange(child_range_size, current_offset))
                current_offset += child_range_size
        else:
            print("ERROR: DepthRange too small")

    def get_children(self) -> Iterable[UiObject]:
        l: list[UiObject] = []
        l.extend(x.ui_object for x in reversed(self._top_list))
        l.extend(x.ui_object for x in self._between_list)
        l.extend(x.ui_object for x in self._bot_list)
        l.extend(x.ui_object for x in reversed(self._component_list))
        return l

    def get_click_needing_children(self) -> Iterable[UiObject]:
        return self._click_needing_children

    def get_mouse_over_needing_children(self) -> Iterable[UiObject]:
        return self._mouse_over_needing_children

    def _redetermine_needing_children(self) -> None:
        self._click_needing_children = [c for c in self.get_children() if c.descendant_needs_click]
        self._mouse_over_needing_children = [c for c in self.get_children() if c.descendant_needs_mouse_over]

    def register_child(self, child: UiObject, **kwargs) -> None:
        child_dm: DepthManager = child.depth_manager
        if "position" in kwargs:
            position: ChildDepthUpdatePosition = kwargs["position"]
            if position in [ChildDepthUpdatePosition.ABOVE_X, ChildDepthUpdatePosition.BELOW_X]:
                if "X" in kwargs:
                    relative_child_dm: DepthManager = kwargs["X"].depth_manager
                    if position == ChildDepthUpdatePosition.ABOVE_X:
                        self._insert_child_above(child_dm, relative_child_dm)
                    else:
                        self._insert_child_below(child_dm, relative_child_dm)
                else:
                    self._between_list.append(child_dm)
            elif position == ChildDepthUpdatePosition.ON_TOP:
                self._top_list.append(child_dm)
            elif position == ChildDepthUpdatePosition.ON_BOT:
                self._bot_list.append(child_dm)
            elif position == ChildDepthUpdatePosition.ARBITRARY:
                self._between_list.append(child_dm)
            elif position == ChildDepthUpdatePosition.COMPONENT_ON_TOP:
                self._component_list.append(child_dm)
            elif position == ChildDepthUpdatePosition.COMPONENT_ON_BOT:
                self._component_list.insert(0, child_dm)
            else:
                print("ERROR: Wrong position argument for child registration")
        else:
            self._between_list.append(child_dm)
        child_dm.parent_dm = self
        self.upward_pass(child_dm, child_needs_downward_pass=True)
        self._redetermine_needing_children()

    def _insert_child_below(self, child_dm: DepthManager, relative_child_dm: DepthManager) -> None:
        if relative_child_dm in self._top_list:
            self._top_list.insert(self._top_list.index(relative_child_dm), child_dm)
        elif relative_child_dm in self._between_list:
            self._bot_list.insert(0, child_dm)
        elif relative_child_dm in self._bot_list:
            self._bot_list.insert(self._bot_list.index(relative_child_dm) + 1, child_dm)
        elif relative_child_dm in self._component_list:
            self._component_list.insert(self._component_list.index(relative_child_dm), child_dm)
        else:
            print("ERROR: Relative UiObject is not registered as a child")

    def _insert_child_above(self, child_dm: DepthManager, relative_child_dm: DepthManager) -> None:
        if relative_child_dm in self._top_list:
            self._top_list.insert(self._top_list.index(relative_child_dm) + 1, child_dm)
        elif relative_child_dm in self._between_list:
            self._top_list.insert(0, child_dm)
        elif relative_child_dm in self._bot_list:
            self._bot_list.insert(self._bot_list.index(relative_child_dm), child_dm)
        elif relative_child_dm in self._component_list:
            self._component_list.insert(self._component_list.index(relative_child_dm) + 1, child_dm)
        else:
            print("ERROR: Relative UiObject is not registered as a child")

    def unregister_child(self, child: UiObject) -> None:
        child_dm: DepthManager = child.depth_manager
        try:
            self._top_list.remove(child_dm)
        except ValueError:
            pass
        try:
            self._between_list.remove(child_dm)
        except ValueError:
            pass
        try:
            self._bot_list.remove(child_dm)
        except ValueError:
            pass
        try:
            self._component_list.remove(child_dm)
        except ValueError:
            pass
        child_dm.parent_dm = None
        child_dm.downward_pass(None)
        self._redetermine_needing_children()

    def update_child_depth(self, child: UiObject, **kwargs) -> None:
        self.unregister_child(child)
        self.register_child(child, **kwargs)

    def children_mouse_needs_changed(self, child: DepthManager) -> None:
        self._redetermine_needing_children()
