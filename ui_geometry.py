from __future__ import annotations
from typing import NamedTuple


class Point2i(NamedTuple):
    x: int
    y: int


class Rectangle(NamedTuple):
    xmin: int
    xmax: int
    ymin: int
    ymax: int

    def __contains__(self, p: Point2i) -> bool:
        return self.xmin <= p.x <= self.xmax and self.ymin <= p.y <= self.ymax


class Transformation(NamedTuple):
    dx: int
    dy: int

    def __call__(self, o: Point2i | Rectangle) -> Point2i | Rectangle:
        if isinstance(o, Point2i):
            return Point2i(o.x + self.dx, o.y + self.dy)
        return Rectangle(o.xmin + self.dx, o.xmax + self.dx, o.ymin + self.dy, o.ymax + self.dy)

    def __invert__(self) -> Transformation:
        return Transformation(-self.dx, -self.dy)

    def __mul__(self, other: Transformation) -> Transformation:
        return Transformation(self.dx + other.dx, self.dy + other.dy)
