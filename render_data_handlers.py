from __future__ import annotations

from rendering import RenderDataHandler, Renderable, DataSpecification
from gl_bindings import VertexBufferObject, ElementBufferObject


class BasicRenderDataHandler(RenderDataHandler):
    _element_count: int
    _renderables: list[Renderable]
    _renderable_count: int
    _renderables_changed: bool
    _dynamic_data_changed: bool

    def __init__(self, static_data_specs: list[DataSpecification], dynamic_data_specs: list[DataSpecification],
                 stream_data_specs: list[DataSpecification], static_vbos: list[VertexBufferObject],
                 dynamic_vbos: list[VertexBufferObject], stream_vbos: list[VertexBufferObject],
                 ebo: ElementBufferObject):
        RenderDataHandler.__init__(self, static_data_specs, dynamic_data_specs, stream_data_specs, static_vbos,
                                   dynamic_vbos, stream_vbos, ebo)
        self._renderables = []
        self._renderable_count = 0
        self._renderables_changed = True
        self._dynamic_data_changed = True
        self._element_count = 0

    def register_renderable(self, renderable: Renderable) -> None:
        self._renderables.append(renderable)
        renderable.set_render_data_handler(self, self._renderable_count)
        self._renderable_count += 1
        self._renderables_changed = True

    def unregister_renderable(self, renderable: Renderable) -> None:
        render_id: int = renderable.id_from_render_data_handler
        top_renderable: Renderable = self._renderables.pop()
        if render_id != self._renderable_count - 1:
            self._renderables[render_id] = top_renderable
            top_renderable.set_render_data_handler(self, render_id)
        renderable.set_render_data_handler(None, 0)
        self._renderable_count -= 1
        self._renderables_changed = True

    def notify_about_change_of_dynamic_data(self, renderable_id: int, data_field_index: int = -1, data=None) -> None:
        self._dynamic_data_changed = True

    def finalize_changes(self) -> None:
        if self._renderables_changed:
            self._update_static_data()
            self._update_dynamic_data()
            self._update_element_indices()
        elif self._dynamic_data_changed:
            self._update_dynamic_data()
        if self._stream_data_specification:
            self._update_stream_data()
        self._renderables_changed = False
        self._dynamic_data_changed = False

    def get_element_count(self) -> int:
        return self._element_count

    def _update_static_data(self) -> None:
        data = []
        for _ in self._static_data_specification:
            data.append([])
        for renderable in self._renderables:
            data_slice = renderable.get_static_data()
            for a, b in zip(data_slice, data):
                b.extend(a)
        for d, vbo in zip(data, self._static_vbos):
            vbo.fill(d)

    def _update_dynamic_data(self) -> None:
        data = []
        for _ in self._dynamic_data_specification:
            data.append([])
        for renderable in self._renderables:
            data_slice = renderable.get_dynamic_data()
            for a, b in zip(data_slice, data):
                b.extend(a)
        for d, vbo in zip(data, self._dynamic_vbos):
            vbo.fill(d)

    def _update_stream_data(self) -> None:
        data = []
        for _ in self._stream_data_specification:
            data.append([])
        for renderable in self._renderables:
            data_slice = renderable.get_stream_data()
            for a, b in zip(data_slice, data):
                b.extend(a)
        for d, vbo in zip(data, self._stream_vbos):
            vbo.fill(d)

    def _update_element_indices(self) -> None:
        indices: list[int] = []
        next_free_index: int = 0
        for renderable in self._renderables:
            r_indices: list[int] = renderable.get_element_indices()
            indices.extend(next_free_index + i for i in r_indices)
            next_free_index += renderable.get_vertex_count()
        self._ebo.fill(indices)
        self._element_count = len(indices)
