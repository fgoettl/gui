from __future__ import annotations
from enum import Enum

import pyglet.gl as GL
import ctypes as C

from gl_bindings import ShaderStorageBufferObject
from render_data_handlers import BasicRenderDataHandler
from rendering import Renderable, DataSpecification, AbsoluteRenderer, UiElementRenderer
from shader_program import ShaderProgram


class RenderType(Enum):
    NON_RENDER = 1
    TEST_RECTANGLE_RENDERER = 2


def create_renderer(render_type: RenderType, window_width: int, window_height: int) -> AbsoluteRenderer:
    if render_type == RenderType.NON_RENDER:
        return NonRenderer()
    elif render_type == RenderType.TEST_RECTANGLE_RENDERER:
        return TestRectangleRenderer(window_width, window_height)


class NonRenderer(UiElementRenderer):
    def __init__(self):
        pass

    def render(self) -> None:
        pass

    def register_renderable(self, renderable: Renderable) -> None:
        pass

    def set_window_dimensions(self, window_width: int, window_height: int) -> None:
        pass

    def bind_ssbo(self, ssbo: ShaderStorageBufferObject, binding_location: int) -> None:
        pass


class TestRectangleRenderer(UiElementRenderer):
    def __init__(self, window_width: int, window_height: int):
        shader_program: ShaderProgram = ShaderProgram.create_from_paths("shaders/bordered_rectangle.vs",
                                                                        "shaders/bordered_rectangle.fs",
                                                                        "shaders/bordered_rectangle.gs")
        position_spec: DataSpecification = DataSpecification("position", C.c_int, GL.GL_INT, 2, 1)
        size_spec: DataSpecification = DataSpecification("size", C.c_int, GL.GL_INT, 2, 3)
        color_spec: DataSpecification = DataSpecification("color", C.c_float, GL.GL_FLOAT, 4, 2)
        UiElementRenderer.__init__(self, [position_spec, size_spec], [color_spec], [], shader_program,
                                   BasicRenderDataHandler, GL.GL_POINTS)
        self.set_window_dimensions(window_width, window_height)

    def set_window_dimensions(self, window_width: int, window_height: int) -> None:
        self.set_uniform_int("_window_width", window_width)
        self.set_uniform_int("_window_height", window_height)
