from __future__ import annotations
import ctypes as C
from PIL import Image

c_string = C.POINTER(C.c_char)


def string_to_c(string: str) -> c_string:
    return C.cast(C.c_char_p(bytes(string, "utf-8")), c_string)


def read_file(path: str) -> str:
    content: str = ""
    with open(path, "r") as file:
        content = file.read()
    return content


def load_picture(path: str) -> (int, int, list[float]):
    with Image.open(path) as im:
        pixel_access = im.load()
        width, height = im.size
        data: list[int] = []
        for j in reversed(range(height)):
            for i in range(width):
                data.extend(pixel_access[i, j])
                data.append(255)
    return width, height, data
