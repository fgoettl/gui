from __future__ import annotations
import pyglet
from math import sin, cos
from utility import load_picture
from gl_bindings import Texture2D

from external_window import ExternalWindow
from ui_geometry import Transformation, Rectangle, Point2i
from ui_rendering import UiObject, RenderType, ChildDepthUpdatePosition, MouseSettings
from user_input import MouseEvent
from color import Color, RED, GREEN, BLUE, GRAY30, mix, BLACK


# TODO (user input):
#   - auf Mouse und Keyboard Input hören ermöglichen

# TODO (bald):
#   - Komponenten haben im Moment das gleiche bounding_rectangle wie normale Kinder.
#     Meist bräuchten sie aber ein anderes oder gar keins.
#   - Viele Attribute sind nicht sehr klar und aussagekräftig -> Klarere Namen oder Refactor.
#   - Für GDFs auch andere Optionen als GL.GL_DYNAMIC_DRAW erlauben?
#   - Tatsächliche UiObjekte und entsprechende Renderer anlegen
#   - Layout Manager
#   - UiObject: Möglichkeit von Komponenten über den wirklichen Kindern

# TODO (später):
#   - Möglichkeit hinzufügen, UiObjekte als "fix" zu kennzeichnen, sodass diese dann nicht jedes mal neu an der
#     Berechnung der GDF im Shader teilnehmen müssen.
#   - ShaderProgram: Fehlerbehandlung bei Erstellung
#   - Matrix Typ spezifizieren
#   - ExternalWindow: Umgehen mit Resize oder status_bar = False
#   - Bibliothek daraus erstellen
#   - Können wir übersichtlicher mit den globalen Daten verfahren? (im Shader) Wie kapseln wir das möglichst gut vom
#     eigentlich relevanten Teil des Shaders ab?
#   - Namen für get_static_data_without_id überdenken
#   - Umgang mit Mouseover bei Entfernen und erneutem Anhängen von Kindern überdenken.
#     Aktuell: Hat nach dem neuen Anhängen den Status, den es beim Entfernen hatte.
#   - Evaluieren: Ist es gut, mouse position in geplanten Abständen zu aktualisieren?
#     Oder sollte es doch nur bei Mausbewegungen sein? Effizienz?
#     (Betrifft nur Over/Enter/Exit, da Clicks separat behandelt werden.)
#   - RenderType so erweitern, dass Renderer und Renderables überprüft werden können, ob sie sich an das Format halten.

class UiRectangle(UiObject):
    def __init__(self, color: Color, text: str):
        UiObject.__init__(self, RenderType.TEST_RECTANGLE_RENDERER)
        self.color = color
        self.original_color = color
        self.text = text
        self.bounding_rectangle = Rectangle(10, 390, 10, 290)
        self.mouse_settings = MouseSettings(True, True, True)

    def get_vertex_count(self) -> int:
        return 1

    def get_static_data_without_id(self):
        positions = [0, 0]
        positions = [p * 40 for p in positions]
        size = [400, 300]
        return [positions, size]

    def get_dynamic_data(self):
        color = self.color
        return [color]

    def get_stream_data(self):
        return []

    def get_element_indices(self) -> list[int]:
        return [0]

    def _mouse_hit(self, mouse_position: Point2i) -> bool:
        x, y = mouse_position
        return 0 <= x <= 400 and 0 <= y <= 300

    def on_mouse_button_down(self, mouse_event: MouseEvent) -> None:
        print("PRESS: ", self.text)

    def on_mouse_button_up(self, mouse_event: MouseEvent) -> None:
        print("RELEASE: ", self.text)

    def on_mouse_click(self, mouse_event: MouseEvent) -> None:
        print("CLICKED: ", self.text)
        self.clickable = False
        self.mouse_over_enabled = False
        self._parent.remove_child(self)

    def on_mouse_enter(self) -> None:
        self.color = mix(self.original_color, BLACK)
        self.dynamic_data_changed()

    def on_mouse_exit(self) -> None:
        self.color = self.original_color
        self.dynamic_data_changed()

    def on_mouse_over(self, mouse_position: Point2i) -> None:
        pass


class Turner:
    def __init__(self, rects):
        self.rects = rects
        self.t = 0
        self.state = 0

    def update(self, dt):
        self.t += dt/3
        self.rects[0].transformation = Transformation(int(400 * sin(0.9 * self.t)), int(400 * cos(self.t)))
        self.rects[2].transformation = Transformation(int(40 * sin(self.t / 0.6)), int(40 * cos(self.t / 0.7)))
        self.rects[1].transformation = Transformation(int(100 * sin(0.2 * self.t)), int(100 * cos(0.67 * self.t)))
        if self.t > 2 and self.state == 0:
            self.rects[3].remove_child(self.rects[1])
            self.state = 1
        if self.t > 10 and self.state == 1:
            self.state = 2
            self.rects[0].add_child(self.rects[1])


def gen_rectangles():
    r1 = UiRectangle(RED, "rot")
    r2 = UiRectangle(GREEN, "gruen")
    r3 = UiRectangle(BLUE, "blau")
    r4 = UiRectangle(GRAY30, "grau")
    r2.add_child(r1, position=ChildDepthUpdatePosition.COMPONENT_ON_TOP)
    r2.add_child(r4, position=ChildDepthUpdatePosition.ON_BOT)
    r1.add_child(r3)
    r1.transformation = Transformation(60, 0)
    r2.transformation = Transformation(400, 400)
    r3.transformation = Transformation(-100, 90)
    r4.transformation = Transformation(-20, -50)
    return [r4, r1, r3, r2]


def main():
    win = ExternalWindow()

    w, h, d = load_picture("textures/texture1.bmp")
    tex1 = Texture2D(w, h, d)
    tex1.bind_to_texture_unit(0)

    rects = gen_rectangles()
    win.add_child(rects[3])
    t = Turner(rects)
    win.schedule_interval(t.update, 1 / 60)
    pyglet.app.run()


if __name__ == '__main__':
    main()
