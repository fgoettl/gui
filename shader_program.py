from __future__ import annotations
import ctypes as C
import weakref

import pyglet.gl as GL

from utility import read_file, string_to_c, c_string

Shader = C.c_uint
Program = C.c_uint
UniformLocation = C.c_int


def check_shader_compilation_status(shader: Shader) -> None:  # TODO Rueckgabe und Fehlerbehandlung
    status: C.c_int = C.c_int()
    GL.glGetShaderiv(shader, GL.GL_COMPILE_STATUS, C.byref(status))
    if status.value != 1:
        info_log: C.Array[C.c_char] = C.create_string_buffer(512)
        GL.glGetShaderInfoLog(shader, 512, None, info_log)
        print("ERROR: Shader compilation failed: \n", info_log.value)


def construct_shader(shader_source: str, shader_type: GL.GLenum) -> Shader:
    shader: Shader = GL.glCreateShader(shader_type)
    c_shader_source: c_string = string_to_c(shader_source)
    GL.glShaderSource(shader, 1, C.byref(c_shader_source), None)
    GL.glCompileShader(shader)
    check_shader_compilation_status(shader)
    return shader


def check_program_link_status(program: Program) -> None:  # TODO Rueckgabe und Fehlerbehandlung
    status: C.c_int = C.c_int()
    GL.glGetProgramiv(program, GL.GL_LINK_STATUS, C.byref(status))
    if status.value != 1:
        info_log: C.Array[C.c_char] = C.create_string_buffer(512)
        GL.glGetProgramInfoLog(program, 512, None, info_log)
        print("ERROR: ShaderProgram linking failed: \n", info_log.value)


def construct_program(*shaders: Shader) -> Program:
    shader_program: Program = GL.glCreateProgram()
    for shader in shaders:
        GL.glAttachShader(shader_program, shader)
    GL.glLinkProgram(shader_program)
    check_program_link_status(shader_program)
    return shader_program


def delete_program(program: Program) -> None:
    GL.glDeleteProgram(program)


class ShaderProgram:
    _shader_program: Program
    _uniform_dict: dict[str, UniformLocation]

    @staticmethod
    def create_from_paths(vertex_shader_path: str, fragment_shader_path: str,
                          geometry_shader_path: str | None = None) -> ShaderProgram:
        vertex_shader_source: str = read_file(vertex_shader_path)
        fragment_shader_source: str = read_file(fragment_shader_path)
        geometry_shader_source: str | None = None if (geometry_shader_path is None) else read_file(geometry_shader_path)
        return ShaderProgram(vertex_shader_source, fragment_shader_source, geometry_shader_source)

    @staticmethod
    def create_from_sources(vertex_shader_source: str, fragment_shader_source: str,
                            geometry_shader_source: str | None = None) -> ShaderProgram:
        return ShaderProgram(vertex_shader_source, fragment_shader_source, geometry_shader_source)

    def __init__(self, vertex_shader_source: str, fragment_shader_source: str, geometry_shader_source: str | None):
        vertex_shader: Shader = construct_shader(vertex_shader_source, GL.GL_VERTEX_SHADER)
        fragment_shader: Shader = construct_shader(fragment_shader_source, GL.GL_FRAGMENT_SHADER)
        if geometry_shader_source is not None:
            geometry_shader: Shader = construct_shader(geometry_shader_source, GL.GL_GEOMETRY_SHADER)
            self._shader_program = construct_program(vertex_shader, geometry_shader, fragment_shader)
            GL.glDeleteShader(geometry_shader)
        else:
            self._shader_program = construct_program(vertex_shader, fragment_shader)
        GL.glDeleteShader(vertex_shader)
        GL.glDeleteShader(fragment_shader)
        self._uniform_dict = dict()
        weakref.finalize(self, delete_program, self._shader_program)

    def __enter__(self) -> None:
        GL.glUseProgram(self._shader_program)

    def __exit__(self, type, value, traceback) -> None:
        pass

    def set_uniform_bool(self, name: str, value: bool) -> None:
        if name not in self._uniform_dict:
            self._uniform_dict[name] = GL.glGetUniformLocation(self._shader_program, string_to_c(name))
        GL.glUniform1i(self._uniform_dict[name], C.c_int(value))

    def set_uniform_int(self, name: str, value: int) -> None:
        if name not in self._uniform_dict:
            self._uniform_dict[name] = GL.glGetUniformLocation(self._shader_program, string_to_c(name))
        GL.glUniform1i(self._uniform_dict[name], C.c_int(value))

    def set_uniform_float(self, name: str, value: float) -> None:
        if name not in self._uniform_dict:
            self._uniform_dict[name] = GL.glGetUniformLocation(self._shader_program, string_to_c(name))
        GL.glUniform1f(self._uniform_dict[name], C.c_float(value))

    def set_uniform_float4(self, name: str, value1: float, value2: float, value3: float, value4: float) -> None:
        if name not in self._uniform_dict:
            self._uniform_dict[name] = GL.glGetUniformLocation(self._shader_program, string_to_c(name))
        GL.glUniform1i(self._uniform_dict[name], C.c_float(value1), C.c_float(value2), C.c_float(value3),
                       C.c_float(value4))

    def set_uniform_mat4(self, name: str, matrix) -> None:  # TODO Matrix Typ spezifizieren
        if name not in self._uniform_dict:
            self._uniform_dict[name] = GL.glGetUniformLocation(self._shader_program, string_to_c(name))
        GL.glUniformMatrix4fv(self._uniform_dict[name], 1, GL.GL_TRUE,
                              (C.c_float * len(matrix))(*matrix))
